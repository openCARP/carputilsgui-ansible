#!/usr/bin/env python
import yaml

data = yaml.safe_load(open('vars.yml'))
external_url = data.get('external_url', '')
instances = data.get('instances', [])

for instance in instances:
    print(external_url.rstrip('/') + '/' + instance.get('index') + '/')
    print('username:', instance.get('username'))
    print('password:', instance.get('password'))
    print()
