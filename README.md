# carputilsGUI-ansible

## Setup

1) Install ansible in a virtual env

    ```
    Install ansible in a virtual env
    python3 -m venv env
    source env/bin/activate
    pip install -r requirements.txt
    ```

2) Create `hosts` file containing the target hostname, e.g.

```
example.com
```

See `hosts.sample` for a sample file.


3) Create `main.yml` with secret usernames and passwords:

```
instances:

- index: '01'
  username: opencarp01
  password: opencarp01

- index: '02'
  username: opencarp02
  password: opencarp02
```

If the public URL differs from the name of the host in the `hosts` file, add an entry for `external_url`:

```
external_url: https://proxy.example.com
```

See `vars.yml.sample` for a sample file.


4) Run `ansible-playbook` using the `play.sh` script:

```
./play.sh

./play.sh -t up       # only run docker-compose up
./play.sh -t stop     # only run docker-compose stop
./play.sh -t down     # only run docker-compose down
./play.sh -t destroy  # only run docker-compose down --volumes
```

5) (optional) Create a list of URLs to send via email:

```
./urls.py
```
